#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

"""
Temporary solution that bridges python2 one wire
libraries via zmq to outer world.
"""

import logging

LOGGER = logging.getLogger('ow_bridge')


class OwProxyError(Exception):
    pass


class OwProxyClientError(OwProxyError):
    pass


class OwProxyServerError(OwProxyError):
    pass


DEFAULT_ZMQ_ADDRESS = "tcp://127.0.0.1:4305"


class OwProxy:

    DEFAULT_OW_SERVER = "localhost:4304"
    OW_HANDLERS = (
        "list_sensors",
        "list_entries",
        "read_entry",
        "write_entry",
    )

    def __init__(self, ow_server=None):
        self.ow_server = ow_server or self.DEFAULT_OW_SERVER
        self._ow = __import__('ow')
        self._ow.init(self.ow_server)

    def _get_sensor(self, path="/", use_cache=True):
        sensor = self._ow.Sensor(str(path))
        self._set_cache(sensor, use_cache)
        return sensor

    @staticmethod
    def _set_cache(sensor, use_cache=True):
        sensor.useCache(use_cache)

    def handle_msg(self, msg):
        if msg.get("action") in self.OW_HANDLERS:
            try:
                return getattr(self, msg.pop("action"))(**msg)
            except Exception, exc:
                raise OwProxyServerError(exc.message)
        raise OwProxyClientError("Unknown action requested.")

    # Callable actions

    def list_sensors(self, path="/", use_cache=True):
        LOGGER.debug("List sensors: %s, %s", path, use_cache)
        sensor = self._get_sensor(path, use_cache)
        return [s._path for s in sensor.sensors()]

    def list_entries(self, path="/", use_cache=True):
        LOGGER.debug("List entries: %s, %s", path, use_cache)
        sensor = self._get_sensor(path, use_cache)
        return sensor.entryList()

    def read_entry(self, path="/", use_cache=True, entry_name="id"):
        LOGGER.debug("Read entry: %s, %s, %s", path, use_cache, entry_name)
        sensor = self._get_sensor(path, use_cache)
        return getattr(sensor, str(entry_name))

    def write_entry(self, value, path="/", use_cache=True, entry_name="id"):
        LOGGER.debug("Write entry: %s, %s, %s", path, use_cache, entry_name)
        sensor = self._get_sensor(path, use_cache)
        return setattr(sensor, str(entry_name), str(value))
