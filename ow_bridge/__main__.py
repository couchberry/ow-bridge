#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

"""
ow_bridge - opens your owserver to the world via zmq REQ-REP socket.

ow_bridge listen on zmq socket for json messages and translate them
into owserver commands. For now, it is internally using python ow
bindings to communicate with owserver. It's just workaround for python3
projects which can't use python2 only ow bindings.
"""

import sys
import zmq
import logging
import argparse
import ow_bridge

# fix for CI where ow module is not installed
try:
    import ow
    OwInitError = ow.exNoController
except ImportError:
    OwInitError = Exception

LOGGER = ow_bridge.LOGGER

def build_arg_parser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        prog='ow_bridge',
    )
    parser.add_argument(
        '--ow', default=ow_bridge.OwProxy.DEFAULT_OW_SERVER,
        dest='ow_server', metavar='OWSERVER',
        type=str, help="ow-server's address",
    )
    parser.add_argument(
        '--zmq', default=ow_bridge.DEFAULT_ZMQ_ADDRESS,
        dest='zmq_address', metavar='ZMQ_ADDRESS',
        type=str, help="Where to bind zmq REP-REQ socket.",
    )
    parser.add_argument(
        '--log-level', default='INFO',
        dest='log_level', metavar='LOG_LEVEL',
        type=str, help=(
            "Set logging level for ow_bridge logger. "
            "One of DEBUG, INFO, WARNING, ERROR."
        ),
    )
    return parser


def main(args=None):
    if args is None:
        args = sys.argv[1:]
    cli_args = build_arg_parser().parse_args(args)

    try:
        LOGGER.setLevel(getattr(logging, cli_args.log_level))
    except AttributeError, ValueError:
        sys.exit((
            "Wrong log level: %s."
            " Use one of DEBUG, INFO, WARNING, ERROR"
        )  % cli_args.log_level)

    if not LOGGER.handlers:
        logging.basicConfig(
            format="[%(asctime)s][%(name)s][%(levelname)s] %(message)s"
        )

    LOGGER.info("ow bridge is starting ...")
    LOGGER.info("ow server address: %s", cli_args.ow_server)
    LOGGER.info("zmq socket address: %s", cli_args.zmq_address)

    try:
        zmq_ctx = zmq.Context()
        zmq_rep = zmq_ctx.socket(zmq.REP)
        zmq_rep.bind(cli_args.zmq_address)
        ow_proxy = ow_bridge.OwProxy(cli_args.ow_server)

        while True:
            try:
                msg = zmq_rep.recv_json()
                LOGGER.debug("Received: %s", msg)
                zmq_rep.send_json(ow_proxy.handle_msg(msg))
            except ow_bridge.OwProxyError, exc:
                zmq_rep.send_json({"error": exc.message})

    except OwInitError:
        LOGGER.error((
            "Unable to connect to owserver. "
            "Please check if it's accesible at `%s`"
        ) % cli_args.ow_server)
    except Exception:
        LOGGER.exception("Unhandled exception:")
    finally:
        zmq_rep.close()
        zmq_ctx.term()


if __name__ == "__main__":
    main()
