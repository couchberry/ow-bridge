#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8


import sys
import pytest
import ow_bridge
from ow_bridge.__main__ import build_arg_parser
from pretend import stub, raiser


def test_init(monkeypatch):
    ow = stub(init=lambda x: None)
    monkeypatch.setitem(sys.modules, "ow", ow)
    proxy = ow_bridge.OwProxy()
    assert proxy.ow_server == ow_bridge.OwProxy.DEFAULT_OW_SERVER
    ow_address = "test_address"
    proxy = ow_bridge.OwProxy(ow_address)
    assert proxy.ow_server == ow_address


def test_cli_arg_parsing():
    parser = build_arg_parser()
    assert parser
    args = parser.parse_args(["--ow=localhost:1", "--zmq=lol"])
    assert args.ow_server == "localhost:1"
    assert args.zmq_address == "lol"
    args = parser.parse_args(["--zmq=lol"])
    assert args.ow_server == ow_bridge.OwProxy.DEFAULT_OW_SERVER
    assert args.zmq_address == "lol"


def test_unknown_action(monkeypatch):
    ow = stub(init=lambda x: None)
    monkeypatch.setitem(sys.modules, "ow", ow)
    proxy = ow_bridge.OwProxy()
    with pytest.raises(ow_bridge.OwProxyClientError):
        proxy.handle_msg({})
    with pytest.raises(ow_bridge.OwProxyClientError):
        proxy.handle_msg({"action": "nonexistingaction"})


def test_list_sensors(monkeypatch):
    sensors_paths = ['a', 'b', 'c']
    sensors = [stub(_path=v) for v in sensors_paths]
    ow = stub(
        init=lambda x: None,
        Sensor=lambda *args: stub(
            useCache=lambda x: None,
            sensors=lambda: sensors,
        )
    )
    monkeypatch.setitem(sys.modules, "ow", ow)
    proxy = ow_bridge.OwProxy()
    assert proxy.list_sensors() == sensors_paths
    msg = {"action": "list_sensors"}
    assert proxy.handle_msg(msg) == sensors_paths

    ow = stub(
        init=lambda x: None,
        Sensor=lambda *args: stub(
            useCache=lambda x: None,
            sensors=raiser(Exception),
        )
    )
    monkeypatch.setitem(sys.modules, "ow", ow)
    msg = {"action": "list_sensors"}
    proxy = ow_bridge.OwProxy()
    with pytest.raises(ow_bridge.OwProxyServerError):
        proxy.handle_msg(msg)


def test_list_entries(monkeypatch):
    entries = ['a', 'b', 'c']
    ow = stub(
        init=lambda x: None,
        Sensor=lambda *args: stub(
            useCache=lambda x: None,
            entryList=lambda: entries,
        )
    )
    monkeypatch.setitem(sys.modules, "ow", ow)
    proxy = ow_bridge.OwProxy()
    assert proxy.list_entries() == entries
    msg = {"action": "list_entries"}
    assert proxy.handle_msg(msg) == entries

    ow = stub(
        init=lambda x: None,
        Sensor=lambda *args: stub(
            useCache=lambda x: None,
            entryList=raiser(Exception),
        )
    )
    monkeypatch.setitem(sys.modules, "ow", ow)
    msg = {"action": "list_entries"}
    proxy = ow_bridge.OwProxy()
    with pytest.raises(ow_bridge.OwProxyServerError):
        proxy.handle_msg(msg)


def test_read_entry(monkeypatch):
    entry_value = "lol"
    ow = stub(
        init=lambda x: None,
        Sensor=lambda *args: stub(
            useCache=lambda x: None,
            id=entry_value,
        )
    )
    monkeypatch.setitem(sys.modules, "ow", ow)
    proxy = ow_bridge.OwProxy()
    assert proxy.read_entry() == entry_value
    msg = {"action": "read_entry"}
    assert proxy.handle_msg(msg) == entry_value

    with pytest.raises(ow_bridge.OwProxyServerError):
        msg = {"action": "read_entry", "entry_name": "nonexistingentry"}
        proxy.handle_msg(msg)


def test_write_entry(monkeypatch):
    entry_value = "lol"

    class Sensor:
        def init(*args):
            pass

        def useCache(*args):
            pass

        @property
        def non_writable(self):
            pass

        @non_writable.setter
        def non_writable(self, value):
            print "Tralkalala"
            raise Exception("non writable")

    sensor_inst = Sensor()
    ow = stub(
        init=lambda x: None,
        Sensor=lambda *args: sensor_inst,
    )
    monkeypatch.setitem(sys.modules, "ow", ow)
    proxy = ow_bridge.OwProxy()
    proxy.write_entry(entry_value)
    assert sensor_inst.id == entry_value
    msg = {
        "action": "write_entry",
        "entry_name": "tst",
        "value": entry_value
    }
    assert proxy.handle_msg(msg) is None
    assert sensor_inst.tst == entry_value
